function setBadBrowser(c_name,value,expiredays)
{
    var exdate = new Date();
    exdate.setDate(exdate.getDate()+expiredays);
    document.cookie=c_name+ "=" + escape(value) +
        ((expiredays==null) ? "" : ";expires=" +
            exdate.toGMTString());
}

function getBadBrowser(c_name)
{
    if (document.cookie.length>0){
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1){
            c_start = c_start + c_name.length+1;
            c_end = document.cookie.indexOf(";",c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
}

document.addEventListener('DOMContentLoaded', function(event) {
    var ua = navigator.userAgent,
        M = ua.match(/(msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(M.length && getBadBrowser('browserWarning') != 'seen' ){

        document.body.innerHTML += '<div id="browserWarning" class="row m-0 h-100 w-100">' +
            '  <div class="col align-self-center">' +
            '    <div class="warning-content w-25 mx-auto bg-light p-3 text-center">' +
            '      <p>' +
            '        You are using an unsupported browser, which is why some features do not work properly. Please switch to a supported browser such as Microsoft Edge, Mozilla Firefox, Google Chrome, Opera or Apple Safari. ' +
            '      </p>' +
            '      <a class="btn btn-primary" href="javascript:void(0)" id="warningClose">Okay</a>' +
            '    </div>' +
            '  </div>' +
            '</div>';

        document.getElementById('warningClose').onclick = function(){
            setBadBrowser('browserWarning','seen');
            document.getElementById('browserWarning').classList.add("d-none");
            return false;
        };
    }
});
